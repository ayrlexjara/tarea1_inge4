<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>
<body>

<?php

	mt_srand(time());
	
	$P1 = mt_rand(0,30);
	$P2 = mt_rand(0,20);
	$FINAL = mt_rand(0,50);
	
	echo "Nota del parcial 1: $P1 / 30</br>";
	echo "Nota del parcial 2:  $P2 / 20</br>";
	echo "Nota del final: $FINAL / 50</br></br>";
	
	switch ($suma = ($P1 + $P2 + $FINAL))
	{
		case ( $suma > 0 && $suma < 60 ):
        	echo "Total obtenido: ".($suma)."</br>";
			echo "Calificacion final: 1 (UNO)";
			break;
		
		case ( $suma >= 60 && $suma < 70 ):
        	echo "Total obtenido: ".($suma)."</br>";
			echo "Calificacion final: 2 (DOS)";
			break;
			
		case ( $suma >= 70 && $suma < 80 ):
        	echo "Total obtenido: ".($suma)."</br>";
			echo "Calificacion final: 3 (TRES)";
			break;
			
		case ( $suma >= 80 && $suma < 91 ):
        	echo "Total obtenido: ".($suma)."</br>";
			echo "Calificacion final: 4 (CUATRO)";
			break;
			
		case ( $suma >= 91 && $suma <= 100 ):
        	echo "Total obtenido: ".($suma)."</br>";
			echo "Calificacion final: 5 (CINCO)";
			break;
		
		default:
			echo "SUMA SOBREPASA LA ESCALA, VERIFICAR LOS DATOS INGRESADOS";
			break;
	}	
?>

</body>
</html>


