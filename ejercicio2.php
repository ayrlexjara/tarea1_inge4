<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
		table {
 			 border-collapse: collapse;
             margin: 0 auto;
		}
        tbody tr:nth-child(even){
        	background: #e2efda;
        }
	td:not(:first-child){
		text-align: center;
	}
     </style>
</head>
<body>

	<?php
        $a = [
        [
            'Nombre' => 'Coca Cola',
            'Cantidad' => '100',
            'Precio' => number_format(4500, 0, ",", "."),
        ],
     	[
            'Nombre' => 'Pepsi',
            'Cantidad' => '30',
            'Precio' => number_format(4800, 0, ",", "."),
        ],
        [
            'Nombre' => 'Sprite',
            'Cantidad' => '20',
            'Precio' => number_format(4500, 0, ",", "."),
        ],
        [
            'Nombre' => 'Guaraná',
            'Cantidad' => '200',
            'Precio' => number_format(4500, 0, ",", "."),
        ],
        [
            'Nombre' => 'SevenUp',
            'Cantidad' => '24',
            'Precio' => number_format(4800, 0, ",", "."),
        ],
        [
            'Nombre' => 'Mirinda Naranja',
            'Cantidad' => '56',
            'Precio' => number_format(4800, 0, ",", "."),
        ],
        [
            'Nombre' => 'Mirinda Guaraná',
            'Cantidad' => '89',
            'Precio' => number_format(4800, 0, ",", "."),
        ],
        [
            'Nombre' => 'Fanta Naranja',
            'Cantidad' => '10',
            'Precio' => number_format(4500, 0, ",", "."),
        ],
        [
            'Nombre' => 'Fanta Piña',
            'Cantidad' => '2',
            'Precio' => number_format(4500, 0, ",", "."),
        ]
	    ];
	?>
    
   
    <table border="1">
     <th style="background-color:yellow;" colspan="3">Productos</th>
        	<tr>
                <th bgcolor="Lightgrey">Nombre</th>
                <th bgcolor="Lightgrey">Cantidad</th>
                <th bgcolor="Lightgrey">Precio(Gs)</th>
            </tr>

            <?php
			foreach ( $a as $r ) {
      		  ?>
      			  <tr>
    		    <?php
        foreach ( $r as $v ) {
        ?>
                <td><?php echo $v;?></td>
        <?php
        }
        ?>
        </tr>
		<?php
		}
		?>
	</table>
        
</body>
</html>

