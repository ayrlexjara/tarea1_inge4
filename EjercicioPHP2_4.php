<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

<?php	
	
	$V1 = 24.5;
	echo "Tipo de varible: ".gettype($V1);
	echo "<br/>";
	echo "<br/>";
    
	$V1 = "Hola";
	echo "El tipo de la variable es: ".gettype($V1);
	echo "<br/>";
    echo "<br/>";

	settype($V1, "integer");
	var_dump($V1);	

?>

</body>
</html>